from django.db import models
from organization.models import Organization
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    organization = models.ForeignKey(Organization,null=True,on_delete=models.SET_NULL)

