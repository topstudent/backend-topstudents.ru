from rest_framework import serializers
from news.models import News


class NewDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('id', 'title', 'category', 'text', 'img','created_at')

class NewsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'