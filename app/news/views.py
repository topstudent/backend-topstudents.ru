from rest_framework import generics
from news.serializers import NewDetailsSerializer, NewsListSerializer
from news.models import News
#from events.permmissions import IsOwnerOrReadOnly


class NewCreateView(generics.CreateAPIView):
    serializer_class = NewDetailsSerializer


class NewsListView(generics.ListAPIView):
    serializer_class = NewsListSerializer
    queryset = News.objects.all()


class NewDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NewsListSerializer
    queryset = News.objects.all()
    # permission_classes = (IsOwnerOrReadOnly, )