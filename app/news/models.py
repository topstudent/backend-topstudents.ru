from django.db import models

class News(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок новости')
    category = models.CharField(max_length=45, verbose_name='Категория новости')
    text = models.TextField(verbose_name='Новость')
    img = models.CharField(max_length=255, verbose_name='Картинка к новости')
    created_at = models.DateTimeField(auto_now=True)
    update_at = models.DateTimeField(auto_now_add=True)


