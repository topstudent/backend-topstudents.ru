# Generated by Django 2.2.5 on 2019-10-28 15:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Название мероприятия')),
                ('category', models.CharField(max_length=15, verbose_name='Категория')),
                ('date', models.DateTimeField()),
                ('phone', models.CharField(max_length=20, verbose_name='Номер телефона')),
                ('address', models.CharField(max_length=100, verbose_name='Адрес')),
                ('price', models.IntegerField(verbose_name='price')),
                ('discount_price', models.IntegerField(verbose_name='Скидка по мероприятию')),
                ('img', models.CharField(max_length=255, verbose_name='Картинка к мероприятию')),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('update_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
