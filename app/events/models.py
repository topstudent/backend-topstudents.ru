from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

"""
organization id int
title varchar 100
description text
category varchar 15
date date
time time
phone varchar 20
adrress varchar 100
geoposition varchar 45
price int
description_price int 
checked tinyint
created_at timestamp
apdated_at timestamp

"""

class Events (models.Model):
    title = models.CharField(max_length=100, verbose_name='Название мероприятия')
    description = models.TextField
    category = models.CharField(max_length=15,verbose_name='Категория')
    date = models.DateTimeField()
    phone = models.CharField(max_length=20,verbose_name='Номер телефона')
    address = models.CharField(max_length=100,verbose_name='Адрес')
    price = models.IntegerField(verbose_name='price')
    discount_price = models.IntegerField(verbose_name='Скидка по мероприятию')
    models.BooleanField(verbose_name='Условная отметка что мероприятие завершилось')
    img = models.CharField(max_length=255, verbose_name='Картинка к мероприятию')
    created_at = models.DateTimeField(auto_now=True)
    update_at = models.DateTimeField(auto_now_add=True)



