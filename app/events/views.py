from rest_framework import generics
from events.serializers import EventDetailsSerializer, EventsListSerializer
from events.models import Events
from events.permmissions import IsOwnerOrReadOnly


class EventCreateView(generics.CreateAPIView):
    serializer_class = EventDetailsSerializer


class EventsListView(generics.ListAPIView):
    serializer_class = EventsListSerializer
    queryset = Events.objects.all()


class EventDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EventsListSerializer
    queryset = Events.objects.all()
    # permission_classes = (IsOwnerOrReadOnly, )