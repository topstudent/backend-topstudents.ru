from django.db import models

# Create your models here.

class Discounts(models.Model):
    title = models.CharField(max_length=125, verbose_name='Название скидки')
    description = models.TextField(verbose_name='Описание скидки')
    date_start = models.DateTimeField()
    date_finish = models.DateTimeField()
    phone = models.CharField(max_length=16,verbose_name='Номер телефона')
    address =  models.CharField(max_length=255,verbose_name='Адрес')
    geopositions = models.CharField(max_length=255,verbose_name='Местоположение')
    discount = models.CharField(max_length=20,verbose_name='Скидка')
    checked = models.BooleanField(verbose_name='Условная отметка что акция завершилась')
    created_at = models.DateTimeField(auto_now=True)
    update_at = models.DateTimeField(auto_now_add=True)