from rest_framework import generics
from discounts.serializers import DiscountDetailsSerializer, DiscountsListSerializer
from discounts.models import Discounts
#from events.permmissions import IsOwnerOrReadOnly


class DiscountCreateView(generics.CreateAPIView):
    serializer_class = DiscountDetailsSerializer


class DiscountsListView(generics.ListAPIView):
    serializer_class = DiscountsListSerializer
    queryset = Discounts.objects.all()


class DiscountDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DiscountDetailsSerializer
    queryset = Discounts.objects.all()
    # permission_classes = (IsOwnerOrReadOnly, )