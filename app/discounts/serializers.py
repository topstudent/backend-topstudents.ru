from rest_framework import serializers
from discounts.models import Discounts


class DiscountDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discounts
        fields = ('id', 'title', 'description', 'date_start', 'date_finish','phone','address','geopositions','discount','checked')

class DiscountsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discounts
        fields = '__all__'