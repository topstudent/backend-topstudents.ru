from django.db import models
from news.models import News
from discounts.models import Discounts
from events.models import Events



# Create your models here.

class Organization(models.Model):
    email = models.CharField(max_length=125, verbose_name='Email')
    password = models.CharField(max_length=255, verbose_name='Пароль')
    name = models.CharField(max_length=150, verbose_name='название огранизации')
    descriptions = models.TextField(verbose_name='Описание')
    phone = models.CharField(max_length=16, verbose_name='Номер телефона')
    web_site = models.CharField(max_length=255, verbose_name='Сатй организации')
    vk = models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в VK')
    twitter = models.CharField(max_length=255, verbose_name='Ссылка на twitter')
    ok = models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в OK')
    facebook = models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в facebook')
    img = models.CharField(max_length=255, verbose_name='Аватарка организации')
    news = models.ManyToManyField(News)
    discounts = models.ManyToManyField(Discounts)
    events = models.ManyToManyField(Events)
    created_at = models.DateTimeField(auto_now=True)
    update_at = models.DateTimeField(auto_now_add=True)

