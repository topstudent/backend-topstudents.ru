# Generated by Django 2.2.5 on 2019-10-28 15:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('events', '0001_initial'),
        ('news', '0001_initial'),
        ('discounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=125, verbose_name='Email')),
                ('password', models.CharField(max_length=255, verbose_name='Пароль')),
                ('name', models.CharField(max_length=150, verbose_name='название огранизации')),
                ('descriptions', models.TextField(verbose_name='Описание')),
                ('phone', models.CharField(max_length=16, verbose_name='Номер телефона')),
                ('web_site', models.CharField(max_length=255, verbose_name='Сатй организации')),
                ('vk', models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в VK')),
                ('twitter', models.CharField(max_length=255, verbose_name='Ссылка на twitter')),
                ('ok', models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в OK')),
                ('facebook', models.CharField(max_length=255, verbose_name='Ссылка на группу огранизации в facebook')),
                ('img', models.CharField(max_length=255, verbose_name='Аватарка организации')),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('update_at', models.DateTimeField(auto_now_add=True)),
                ('discounts', models.ManyToManyField(to='discounts.Discounts')),
                ('events', models.ManyToManyField(to='events.Events')),
                ('news', models.ManyToManyField(to='news.News')),
            ],
        ),
    ]
